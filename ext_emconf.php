<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'co-stack.com FAL SFTP Driver',
    'description' => 'Adds a Driver to your TYPO3 that lets you create and connect to a file storage accessible via SFTP',
    'category' => 'be',
    'state' => 'stable',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearcacheonload' => true,
    'author' => 'Oliver Eglseder',
    'author_email' => 'oliver.eglseder@co-stack.com',
    'author_company' => 'co-stack.com',
    'version' => '4.1.2',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-10.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
