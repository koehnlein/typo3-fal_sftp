<?php

/** @noinspection PhpFullyQualifiedNameUsageInspection */

(static function () {
    defined('TYPO3_MODE') || die();

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['FileInfo']['fileExtensionToMimeType']['png'] = 'image/png';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['fal']['registeredDrivers']['Sftp'] = [
        'class' => \CoStack\FalSftp\Driver\SftpDriver::class,
        'flexFormDS' => 'FILE:EXT:fal_sftp/Configuration/FlexForm/DriverConfiguration.xml',
        'label' => 'SFTP Driver',
        'shortName' => 'Sftp',
    ];
})();
