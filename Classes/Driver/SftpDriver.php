<?php

declare(strict_types=1);

namespace CoStack\FalSftp\Driver;

use CoStack\FalSftp\Adapter\AdapterInterface;
use CoStack\FalSftp\Adapter\Phpseclib3Adapter;
use CoStack\FalSftp\Adapter\PhpseclibAdapter;
use CoStack\FalSftp\Adapter\PhpSshAdapter;
use Exception;
use InvalidArgumentException;
use RuntimeException;
use Throwable;
use TYPO3\CMS\Core\Charset\CharsetConverter;
use TYPO3\CMS\Core\Resource\Driver\AbstractHierarchicalFilesystemDriver;
use TYPO3\CMS\Core\Resource\Exception\InvalidConfigurationException;
use TYPO3\CMS\Core\Resource\Exception\InvalidFileNameException;
use TYPO3\CMS\Core\Resource\ResourceStorageInterface;
use TYPO3\CMS\Core\Type\File\FileInfo;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

use function array_keys;
use function array_reverse;
use function array_slice;
use function count;
use function file_get_contents;
use function file_put_contents;
use function in_array;
use function is_callable;
use function ltrim;
use function mt_rand;
use function octdec;
use function parse_url;
use function pathinfo;
use function preg_replace;
use function rtrim;
use function str_replace;
use function strlen;
use function strnatcasecmp;
use function strtolower;
use function substr;
use function touch;
use function trim;
use function uasort;
use function unlink;

use const PATHINFO_EXTENSION;
use const PHP_INT_MAX;

class SftpDriver extends AbstractHierarchicalFilesystemDriver
{
    public const ADAPTER_PHPSSH = 1;
    public const ADAPTER_PHPSECLIB = 2;
    public const ADAPTER_PHPSECLIB3 = 3;
    public const CONFIG_ADAPTER = 'adapter';
    public const CONFIG_AUTHENTICATION_METHOD = 'authenticationMethod';
    public const CONFIG_PUBLIC_URL = 'publicUrl';
    public const CONFIG_FILE_MODE = 'fileCreateMask';
    public const CONFIG_FOLDER_MODE = 'folderCreateMask';
    public const CONFIG_HOSTNAME = 'hostname';
    public const CONFIG_ROOT_LEVEL = 'rootLevel';
    public const CONFIG_PORT = 'port';
    public const CONFIG_USERNAME = 'username';
    public const CONFIG_EXPERTS = 'experts';
    public const CONFIG_FOREIGN_KEY_FINGERPRINT = 'foreignKeyFingerprint';
    public const CONFIG_FOREIGN_KEY_FINGERPRINT_METHOD = 'foreignKeyFingerprintMethod';
    public const UNSAFE_FILENAME_CHARACTER_EXPRESSION = '\\x00-\\x2C\\/\\x3A-\\x3F\\x5B-\\x60\\x7B-\\xBF';

    protected $supportedHashAlgorithms = ['md5', 'sha1'];

    /** @var null|AdapterInterface */
    protected $adapter;

    protected $rootPath = '';

    protected $rootPathLength = 0;

    protected $scheme = 'https://';

    protected $connected = false;

    public function processConfiguration(): void
    {
        $this->rootPath = '/' . trim($this->configuration[self::CONFIG_ROOT_LEVEL] ?? '', '/') . '/';
        $this->rootPathLength = strlen($this->rootPath) - 1;
        if (!empty($this->configuration[self::CONFIG_PUBLIC_URL])) {
            $this->capabilities = ResourceStorageInterface::CAPABILITY_BROWSABLE
                                  | ResourceStorageInterface::CAPABILITY_PUBLIC
                                  | ResourceStorageInterface::CAPABILITY_WRITABLE;
            $parsedUrl = parse_url($this->configuration[self::CONFIG_PUBLIC_URL]);
            if (isset($parsedUrl['scheme'], $parsedUrl['host'])) {
                $publicDomain = $parsedUrl['host'];
                if (isset($parsedUrl['path'])) {
                    $publicDomain .= $parsedUrl['path'];
                }
            } elseif (isset($parsedUrl['path'])) {
                $publicDomain = $parsedUrl['path'];
            } else {
                throw new InvalidConfigurationException('The given public url is not valid', 1476695176);
            }
            $this->configuration[self::CONFIG_PUBLIC_URL] = rtrim($publicDomain, '/') . '/';

            if (true !== (bool)GeneralUtility::getIndpEnv('TYPO3_SSL')) {
                $this->scheme = 'http://';
            }
        } else {
            $this->capabilities = ResourceStorageInterface::CAPABILITY_BROWSABLE
                                  | ResourceStorageInterface::CAPABILITY_WRITABLE;
        }

        $this->processCreateMask(self::CONFIG_FILE_MODE);
        $this->processCreateMask(self::CONFIG_FOLDER_MODE);

        try {
            switch ($this->configuration[self::CONFIG_ADAPTER]) {
                case self::ADAPTER_PHPSSH:
                    $this->adapter = new PhpSshAdapter($this->configuration);
                    break;
                case self::ADAPTER_PHPSECLIB:
                    $this->adapter = new PhpseclibAdapter($this->configuration);
                    break;
                case self::ADAPTER_PHPSECLIB3:
                    $this->adapter = new Phpseclib3Adapter($this->configuration);
                    break;
                default:
                    throw new InvalidArgumentException('Adapter configuration is not supported', 1476629708);
            }
        } catch (Throwable $exception) {
            throw new InvalidConfigurationException($exception->getMessage(), $exception->getCode());
        }
    }

    protected function connectAdapter(): void
    {
        if ($this->connected === false) {
            if (true !== $this->adapter->connect()) {
                throw new InvalidConfigurationException('Could not connect to remote host', 1476629391);
            }

            if (true === $this->configuration[static::CONFIG_EXPERTS]) {
                if (!empty($this->configuration[static::CONFIG_FOREIGN_KEY_FINGERPRINT])) {
                    $actualFingerprint = $this->adapter->getForeignKeyFingerprint(
                        $this->configuration[static::CONFIG_FOREIGN_KEY_FINGERPRINT_METHOD]
                    );
                    if ($this->configuration[static::CONFIG_FOREIGN_KEY_FINGERPRINT] !== $actualFingerprint) {
                        throw new RuntimeException('The foreign key fingerprint does not match', 1476629167);
                    }
                }
            }
            $this->connected = true;
        }
    }

    public function initialize(): void
    {
    }

    public function mergeConfigurationCapabilities($capabilities): int
    {
        $this->capabilities &= $capabilities;
        return $this->capabilities;
    }

    public function getRootLevelFolder(): string
    {
        return '/';
    }

    public function getDefaultFolder(): string
    {
        $defaultFolder = '/user_upload/';
        $identifier = $this->rootPath . $defaultFolder;
        if (!$this->folderExists($identifier)) {
            $this->connectAdapter();
            $this->adapter->createFolder($identifier);
        }
        return $defaultFolder;
    }

    public function getPublicUrl($identifier): string
    {
        return $this->scheme . $this->configuration[self::CONFIG_PUBLIC_URL] . ltrim($identifier, '/');
    }

    public function createFolder($newFolderName, $parentFolderIdentifier = '', $recursive = false): string
    {
        $parentFolderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($parentFolderIdentifier);
        $newFolderName = $this->canonicalizeAndCheckFolderIdentifier($parentFolderIdentifier . $newFolderName);
        $identifier = $this->canonicalizeAndCheckFolderIdentifier($this->rootPath . $newFolderName);
        $this->connectAdapter();
        $this->adapter->createFolder($identifier, $recursive);
        return $newFolderName;
    }

    public function renameFolder($folderIdentifier, $newName): array
    {
        $parentFolder = $this->canonicalizeAndCheckFolderIdentifier(PathUtility::dirname($folderIdentifier));
        $oldIdentifier = $this->canonicalizeAndCheckFolderIdentifier(
            $parentFolder . PathUtility::basename($folderIdentifier)
        );
        $newIdentifier = $this->canonicalizeAndCheckFolderIdentifier(
            $parentFolder . PathUtility::basename($this->sanitizeFileName($newName))
        );
        $oldFolderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($this->rootPath . $oldIdentifier);
        $newFolderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($this->rootPath . $newIdentifier);
        $this->connectAdapter();
        $this->adapter->rename($oldFolderIdentifier, $newFolderIdentifier);
        $items = $this->adapter->scanDirectory($newFolderIdentifier, true, true, true);
        return $this->createIdentifierMap($items, $oldIdentifier, $newIdentifier);
    }

    public function deleteFolder($folderIdentifier, $deleteRecursively = false): bool
    {
        $folderIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $folderIdentifier);
        $this->connectAdapter();
        return $this->adapter->delete($folderIdentifier, $deleteRecursively);
    }

    public function fileExists($fileIdentifier): bool
    {
        $fileIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $fileIdentifier);
        $this->connectAdapter();
        return $this->adapter->exists($fileIdentifier);
    }

    public function folderExists($folderIdentifier): bool
    {
        $folderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($this->rootPath . $folderIdentifier);
        $this->connectAdapter();
        return $this->adapter->exists($folderIdentifier, AdapterInterface::TYPE_FOLDER);
    }

    public function isFolderEmpty($folderIdentifier): bool
    {
        $folderIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $folderIdentifier);
        $this->connectAdapter();
        return count($this->adapter->scanDirectory($folderIdentifier)) === 0;
    }

    public function addFile($localFilePath, $targetFolderIdentifier, $newFileName = '', $removeOriginal = true): string
    {
        $localFilePath = $this->canonicalizeAndCheckFilePath($localFilePath);
        $targetFolderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($targetFolderIdentifier);
        $targetFileIdentifier = $targetFolderIdentifier . $this->sanitizeFileName($newFileName);
        $identifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $targetFileIdentifier);
        $this->connectAdapter();
        if ($this->adapter->uploadFile($localFilePath, $identifier) && $removeOriginal) {
            unlink($localFilePath);
        }
        return $targetFileIdentifier;
    }

    public function createFile($fileName, $parentFolderIdentifier): string
    {
        $parentFolderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($parentFolderIdentifier);
        $fileName = $this->sanitizeFileName($fileName);
        $temporaryFile = GeneralUtility::tempnam(
            'fal-tempfile-',
            '.' . PathUtility::pathinfo($fileName, PATHINFO_EXTENSION)
        );
        touch($temporaryFile);
        $fileName = $parentFolderIdentifier . $fileName;
        $identifier = $this->canonicalizeAndCheckFilePath($this->rootPath . $fileName);
        $this->connectAdapter();
        $this->adapter->uploadFile($temporaryFile, $identifier);
        unlink($temporaryFile);
        return $fileName;
    }

    public function copyFileWithinStorage($fileIdentifier, $targetFolderIdentifier, $fileName): string
    {
        $sourceIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $fileIdentifier);
        /*
         * no need to sanitize the identifier since it has been either
         * sanitized by upload or rename
         */
        $newIdentifier = $targetFolderIdentifier . $fileName;
        $targetIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $newIdentifier);
        $this->connectAdapter();
        $this->adapter->copy($sourceIdentifier, $targetIdentifier);
        return $newIdentifier;
    }

    public function renameFile($fileIdentifier, $newName): string
    {
        $folder = $this->canonicalizeAndCheckFolderIdentifier(PathUtility::dirname($fileIdentifier));

        $identifier = $this->canonicalizeAndCheckFileIdentifier($folder . $this->sanitizeFileName($newName));
        $oldIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $fileIdentifier);
        $newIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $identifier);
        $this->connectAdapter();
        $this->adapter->rename($oldIdentifier, $newIdentifier);
        return $identifier;
    }

    public function replaceFile($fileIdentifier, $localFilePath): bool
    {
        $contents = file_get_contents($localFilePath);
        return mb_strlen($contents) === $this->setFileContents($fileIdentifier, $contents);
    }

    public function deleteFile($fileIdentifier): bool
    {
        $fileIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $fileIdentifier);
        $this->connectAdapter();
        return $this->adapter->delete($fileIdentifier, false);
    }

    public function hash($fileIdentifier, $hashAlgorithm): string
    {
        $fileIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $fileIdentifier);
        $this->connectAdapter();
        return $this->adapter->hash($fileIdentifier, $hashAlgorithm);
    }

    public function moveFileWithinStorage($fileIdentifier, $targetFolderIdentifier, $newFileName): string
    {
        $fileIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $fileIdentifier);
        $newFileIdentifier = $targetFolderIdentifier . $newFileName;
        $targetIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $newFileIdentifier);
        $this->connectAdapter();
        $this->adapter->rename($fileIdentifier, $targetIdentifier);
        return $newFileIdentifier;
    }

    public function moveFolderWithinStorage($sourceFolderIdentifier, $targetFolderIdentifier, $newFolderName): array
    {
        $oldIdentifier = $sourceFolderIdentifier;
        $sourceIdentifier = $this->canonicalizeAndCheckFolderIdentifier($this->rootPath . $sourceFolderIdentifier);
        $newIdentifier = $targetFolderIdentifier . $newFolderName;
        $targetIdentifier = $this->canonicalizeAndCheckFolderIdentifier($this->rootPath . $newIdentifier);
        $this->connectAdapter();
        $this->adapter->rename($sourceIdentifier, $targetIdentifier);
        $items = $this->adapter->scanDirectory($targetIdentifier, true, true, true);
        return $this->createIdentifierMap($items, $oldIdentifier, $newIdentifier);
    }

    public function copyFolderWithinStorage($sourceFolderIdentifier, $targetFolderIdentifier, $newFolderName): bool
    {
        $sourceFolderIdentifier = $this->canonicalizeAndCheckFolderIdentifier(
            $this->rootPath . $sourceFolderIdentifier
        );
        $targetFolderIdentifier = $this->canonicalizeAndCheckFolderIdentifier(
            $this->rootPath . $targetFolderIdentifier . $newFolderName
        );
        $this->connectAdapter();
        return $this->adapter->copy($sourceFolderIdentifier, $targetFolderIdentifier);
    }

    public function getFileContents($fileIdentifier): string
    {
        $fileIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $fileIdentifier);
        $this->connectAdapter();
        return $this->adapter->readFile($fileIdentifier);
    }

    public function setFileContents($fileIdentifier, $contents): int
    {
        $temporaryFile = GeneralUtility::tempnam(
            'fal-tempfile-',
            '.' . PathUtility::pathinfo($fileIdentifier, PATHINFO_EXTENSION)
        );
        $bytes = file_put_contents($temporaryFile, $contents);
        $this->connectAdapter();
        do {
            $temporaryIdentifier = $this->canonicalizeAndCheckFileIdentifier(
                $this->rootPath . 'fal-tempfile-' . str_replace('/', '_', $fileIdentifier) . mt_rand(1, PHP_INT_MAX)
            );
        } while ($this->adapter->exists($temporaryIdentifier));

        $this->adapter->uploadFile($temporaryFile, $temporaryIdentifier);
        unlink($temporaryFile);

        $fileIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $fileIdentifier);
        if ($this->adapter->rename($temporaryIdentifier, $fileIdentifier)) {
            return $bytes;
        }
        return 0;
    }

    public function fileExistsInFolder($fileName, $folderIdentifier): bool
    {
        $folderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($folderIdentifier);
        $identifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $folderIdentifier . $fileName);
        $this->connectAdapter();
        return $this->adapter->exists($identifier);
    }

    public function folderExistsInFolder($folderName, $folderIdentifier): bool
    {
        $folderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($folderIdentifier);
        $identifier = $this->canonicalizeAndCheckFolderIdentifier($this->rootPath . $folderIdentifier . $folderName);
        $this->connectAdapter();
        return $this->adapter->exists($identifier, AdapterInterface::TYPE_FOLDER);
    }

    public function getFileForLocalProcessing($fileIdentifier, $writable = true): string
    {
        $this->connectAdapter();
        $fileIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $fileIdentifier);
        return $this->adapter->downloadFile(
            $fileIdentifier,
            GeneralUtility::tempnam(
                'fal-tempfile-' . ($writable ? 'w' : ''),
                '.' . PathUtility::pathinfo($fileIdentifier, PATHINFO_EXTENSION)
            )
        );
    }

    public function getPermissions($identifier): array
    {
        $identifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $identifier);
        $this->connectAdapter();
        return $this->adapter->getPermissions($identifier);
    }

    public function dumpFileContents($identifier): void
    {
        $this->connectAdapter();
        $this->adapter->dumpFile($this->canonicalizeAndCheckFileIdentifier($this->rootPath . $identifier));
    }

    public function isWithin($folderIdentifier, $identifier): bool
    {
        $folderIdentifier = $this->canonicalizeAndCheckFileIdentifier($folderIdentifier);
        $entryIdentifier = $this->canonicalizeAndCheckFileIdentifier($identifier);
        if ($folderIdentifier === $entryIdentifier) {
            return true;
        }
        // File identifier canonicalization will not modify a single slash so
        // we must not append another slash in that case.
        if ($folderIdentifier !== '/') {
            $folderIdentifier .= '/';
        }
        return GeneralUtility::isFirstPartOfStr($entryIdentifier, $folderIdentifier);
    }

    public function getFileInfoByIdentifier($fileIdentifier, array $propertiesToExtract = []): array
    {
        $originalIdentifier = $fileIdentifier;
        $fileIdentifier = $this->canonicalizeAndCheckFileIdentifier($this->rootPath . $fileIdentifier);
        return $this->getDetails($fileIdentifier, $propertiesToExtract, $originalIdentifier);
    }

    protected function getDetails(string $identifier, array $propertiesToExtract, string $originalIdentifier): array
    {
        if (empty($propertiesToExtract)) {
            $propertiesToExtract = [
                'size',
                'atime',
                'mtime',
                'ctime',
                'mimetype',
                'name',
                'extension',
                'identifier',
                'identifier_hash',
                'storage',
                'folder_hash',
            ];
        }
        $this->connectAdapter();
        $information = $this->adapter->getDetails($identifier);
        $information = $this->enrichInformation($information, $originalIdentifier);
        foreach (array_keys($information) as $property) {
            if (!in_array($property, $propertiesToExtract)) {
                unset($information[$property]);
            }
        }
        return $information;
    }

    protected function enrichInformation(array $information, string $originalIdentifier): array
    {
        $information['name'] = PathUtility::basename($originalIdentifier);
        $information['identifier'] = $originalIdentifier;
        $information['storage'] = $this->storageUid;
        $information['identifier_hash'] = $this->hashIdentifier($originalIdentifier);
        $information['extension'] = strtolower(pathinfo($originalIdentifier, PATHINFO_EXTENSION));
        $information['folder_hash'] = $this->hashIdentifier(
            $this->getParentFolderIdentifierOfIdentifier($originalIdentifier)
        );
        if (empty($information['mimetype'])) {
            $mimeType = $this->guessMimeType($originalIdentifier);
            if (null !== $mimeType) {
                $information['mimetype'] = $mimeType;
            }
        }
        return $information;
    }

    public function getFolderInfoByIdentifier($folderIdentifier): array
    {
        $folderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($folderIdentifier);
        $this->connectAdapter();
        $details = $this->adapter->getDetails($this->rootPath . $folderIdentifier);
        return [
            'identifier' => $folderIdentifier,
            'name' => PathUtility::basename($folderIdentifier),
            'mtime' => $details['mtime'],
            'ctime' => $details['ctime'],
            'storage' => $this->storageUid,
        ];
    }

    public function getFileInFolder($fileName, $folderIdentifier): string
    {
        return $this->canonicalizeAndCheckFileIdentifier($folderIdentifier . '/' . $fileName);
    }

    public function getFilesInFolder(
        $folderIdentifier,
        $start = 0,
        $numberOfItems = 0,
        $recursive = false,
        array $filenameFilterCallbacks = [],
        $sort = '',
        $sortRev = false
    ): array {
        $folderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($this->rootPath . $folderIdentifier);
        $this->connectAdapter();
        $items = $this->adapter->scanDirectory($folderIdentifier, true, false, $recursive);
        return $this->processResults(
            $items,
            $sort,
            $sortRev,
            (int)$start,
            (int)$numberOfItems,
            $filenameFilterCallbacks
        );
    }

    public function getFolderInFolder($folderName, $folderIdentifier): string
    {
        return $this->canonicalizeAndCheckFolderIdentifier($folderIdentifier . '/' . $folderName);
    }

    public function getFoldersInFolder(
        $folderIdentifier,
        $start = 0,
        $numberOfItems = 0,
        $recursive = false,
        array $folderNameFilterCallbacks = [],
        $sort = '',
        $sortRev = false
    ): array {
        $folderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($this->rootPath . $folderIdentifier);
        $this->connectAdapter();
        $items = $this->adapter->scanDirectory($folderIdentifier, false, true, $recursive);
        return $this->processResults(
            $items,
            $sort,
            $sortRev,
            (int)$start,
            (int)$numberOfItems,
            $folderNameFilterCallbacks
        );
    }

    protected function processResults(
        array $items,
        string $sort,
        bool $sortRev,
        int $start,
        int $numberOfItems,
        array $filterCallbacks
    ): array {
        $items = $this->sortItems($items, $sort, $sortRev);
        $items = $this->omitItems($items, $start, $numberOfItems);
        $items = $this->filterItems($items, $filterCallbacks);
        foreach (array_keys($items) as $identifier) {
            $items[$identifier] = $identifier;
        }
        return $this->dropRootPaths($items);
    }

    protected function dropRootPaths(array $items): array
    {
        $newItems = [];
        foreach ($items as $identifier) {
            $identifier = substr($identifier, $this->rootPathLength);
            $newItems[$identifier] = $identifier;
        }
        return $newItems;
    }

    protected function sortItems(array $items, string $sort, bool $sortRev): array
    {
        switch ($sort) {
            case 'file':
                $callback = static function ($left, $right) {
                    return strnatcasecmp($left['name'], $right['name']);
                };
                break;
            case '':
                $callback = static function ($left, $right) {
                    return strnatcasecmp($left['identifier'], $right['identifier']);
                };
                break;
            default:
                throw new Exception('TODO: \CoStack\FalSftp\Driver\SftpDriver::sortItems sort by ' . $sort);
        }
        uasort($items, $callback);
        if ($sortRev) {
            $items = array_reverse($items);
        }
        return $items;
    }

    protected function omitItems(array $items, int $start, int $numberOfItems): array
    {
        if ($numberOfItems > 0) {
            $items = array_slice($items, $start, $numberOfItems);
        }
        return array_slice($items, $start);
    }

    protected function filterItems(array $items, array $filterCallbacks): array
    {
        foreach ($items as $identifier => $info) {
            foreach ($filterCallbacks as $filterCallback) {
                if (is_callable($filterCallback)) {
                    $result = $filterCallback(
                        $info['name'],
                        $identifier,
                        PathUtility::dirname($identifier),
                        [],
                        $this
                    );

                    if ($result === -1) {
                        unset($items[$identifier]);
                        break;
                    }

                    if ($result === false) {
                        throw new RuntimeException(
                            'Could not apply file/folder name filter ' . $filterCallback[0] . '::' . $filterCallback[1],
                            1447600899
                        );
                    }
                }
            }
        }
        return $items;
    }

    public function countFilesInFolder(
        $folderIdentifier,
        $recursive = false,
        array $filenameFilterCallbacks = []
    ): int {
        $folderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($this->rootPath . $folderIdentifier);
        $this->connectAdapter();
        $items = $this->adapter->scanDirectory($folderIdentifier, true, false, $recursive);
        $items = $this->filterItems($items, $filenameFilterCallbacks);
        return count($items);
    }

    public function countFoldersInFolder(
        $folderIdentifier,
        $recursive = false,
        array $folderNameFilterCallbacks = []
    ): int {
        $folderIdentifier = $this->canonicalizeAndCheckFolderIdentifier($this->rootPath . $folderIdentifier);
        $this->connectAdapter();
        $items = $this->adapter->scanDirectory($folderIdentifier, false, true, $recursive);
        $items = $this->filterItems($items, $folderNameFilterCallbacks);
        return count($items);
    }

    public function sanitizeFileName($fileName, $charset = ''): string
    {
        // Handle UTF-8 characters
        if ($GLOBALS['TYPO3_CONF_VARS']['SYS']['UTF8filesystem']) {
            // Allow ".", "-", 0-9, a-z, A-Z and everything beyond U+C0 (latin capital letter a with grave)
            $cleanFileName = preg_replace(
                '/[' . self::UNSAFE_FILENAME_CHARACTER_EXPRESSION . ']/u',
                '_',
                trim($fileName)
            );
        } else {
            // Define character set
            if ('' === $charset) {
                // default for Backend
                $charset = 'utf-8';
                // Check for TYPO3 v7
                if (TYPO3_MODE === 'FE' && !empty($GLOBALS['TSFE']->renderCharset)) {
                    $charset = $GLOBALS['TSFE']->renderCharset;
                }
            }
            // If a charset was found, convert fileName
            $fileName = $this->getCharsetConversion()->specCharsToASCII($charset, $fileName);
            // Replace unwanted characters by underscores
            $cleanFileName = preg_replace(
                '/[' . self::UNSAFE_FILENAME_CHARACTER_EXPRESSION . '\\xC0-\\xFF]/',
                '_',
                trim($fileName)
            );
        }
        // Strip trailing dots and return
        $cleanFileName = rtrim($cleanFileName, '.');
        if ($cleanFileName === '') {
            throw new InvalidFileNameException(
                'File name ' . $fileName . ' is invalid.',
                1320288991
            );
        }
        return $cleanFileName;
    }

    protected function createIdentifierMap(array $items, string $sourceIdentifier, string $targetIdentifier): array
    {
        $identifierMap = [];
        $identifierMap[$sourceIdentifier] = $targetIdentifier;
        foreach ($items as $item) {
            $newIdentifier = substr($item['identifier'], $this->rootPathLength);
            $oldIdentifier = str_replace($targetIdentifier, $sourceIdentifier, $newIdentifier);
            if ($item['type'] === 'dir') {
                $newIdentifier = $this->canonicalizeAndCheckFolderIdentifier($newIdentifier);
                $oldIdentifier = $this->canonicalizeAndCheckFolderIdentifier($oldIdentifier);
            } elseif ($item['type'] === 'file') {
                $newIdentifier = $this->canonicalizeAndCheckFileIdentifier($newIdentifier);
                $oldIdentifier = $this->canonicalizeAndCheckFileIdentifier($oldIdentifier);
            } else {
                continue;
            }
            $identifierMap[$oldIdentifier] = $newIdentifier;
        }
        return $identifierMap;
    }

    protected function processCreateMask(string $mode): void
    {
        if (false === (bool)$this->configuration[static::CONFIG_EXPERTS] || empty($this->configuration[$mode])) {
            $octalString = $GLOBALS['TYPO3_CONF_VARS']['SYS'][$mode];
        } else {
            $octalString = $this->configuration[$mode];
        }
        $this->configuration[$mode] = octdec($octalString);
    }

    protected function guessMimeType(string $originalIdentifier): ?string
    {
        $mimeType = null;
        $fileExtensionToMimeTypeMapping = $GLOBALS['TYPO3_CONF_VARS']['SYS']['FileInfo']['fileExtensionToMimeType'];
        $lowercaseFileExtension = strtolower(pathinfo($originalIdentifier, PATHINFO_EXTENSION));
        if (!empty($fileExtensionToMimeTypeMapping[$lowercaseFileExtension])) {
            $mimeType = $fileExtensionToMimeTypeMapping[$lowercaseFileExtension];
        }

        if (isset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][FileInfo::class]['mimeTypeGuessers'])) {
            $mimeTypeGuessers = $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][FileInfo::class]['mimeTypeGuessers'];
            foreach ($mimeTypeGuessers as $mimeTypeGuesser) {
                $hookParameters = [
                    'mimeType' => &$mimeType,
                ];

                GeneralUtility::callUserFunction($mimeTypeGuesser, $hookParameters, $this);
            }
        }

        if (false === $mimeType) {
            $mimeTypeMapping = include(__DIR__ . '/../../Resources/Private/Data/mimetype.php');
            if (isset($mimeTypeMapping[$lowercaseFileExtension])) {
                $mimeType = $mimeTypeMapping[$lowercaseFileExtension];
            }
        }

        return $mimeType;
    }

    protected function getCharsetConversion(): CharsetConverter
    {
        return GeneralUtility::makeInstance(CharsetConverter::class);
    }
}
