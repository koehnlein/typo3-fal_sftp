<?php

declare(strict_types=1);

namespace CoStack\FalSftp\Adapter;

interface AdapterInterface
{
    const TYPE_FILE = 'file';
    const TYPE_FOLDER = 'folder';
    const AUTHENTICATION_PASSWORD = 1;
    const AUTHENTICATION_PUBKEY = 2;
    const HASHING_MD5 = 'md5';
    const HASHING_SHA1 = 'sha1';

    public function __construct(array $configuration);

    public function connect(): bool;

    public function getForeignKeyFingerprint(string $hashingMethod);

    public function scanDirectory(
        string $identifier,
        bool $files = true,
        bool $folders = true,
        bool $recursive = false
    ): array;

    public function exists(string $identifier, string $type = self::TYPE_FILE): bool;

    public function createFolder(string $identifier, bool $recursive = true): string;

    public function getPermissions(string $identifier): array;

    public function getDetails(string $identifier): array;

    public function hash(string $identifier, string $hashAlgorithm): string;

    public function downloadFile(string $identifier, string $target): string;

    public function uploadFile(string $source, string $identifier): bool;

    public function dumpFile(string $identifier): void;

    public function delete(string $identifier, bool $recursive): bool;

    public function readFile(string $identifier): string;

    public function rename(string $oldIdentifier, string $newIdentifier): bool;

    public function copy(string $sourceIdentifier, string $targetIdentifier): bool;
}
