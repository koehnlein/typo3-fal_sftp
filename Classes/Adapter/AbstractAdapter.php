<?php

declare(strict_types=1);

namespace CoStack\FalSftp\Adapter;

use TYPO3\CMS\Core\Utility\PathUtility;

abstract class AbstractAdapter implements AdapterInterface
{
    protected function getShortInfo(string $identifier, string $type): array
    {
        return [
            'identifier' => $identifier,
            'name' => PathUtility::basename($identifier),
            'type' => $type,
        ];
    }
}
